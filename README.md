Astro-LoFAR
========================

**Astro-LoFAR** is a Python package to image fits files, extract spectral information, and handle CASA's MS.


Check the documentation [here](https://astro-lofar.readthedocs.io/en/latest/).
