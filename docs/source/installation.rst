Installation
=======================

To use Astro-LoFAR, first download the package:

.. code-block:: console

	$ git clone https://gitlab.com/ececcotti/astro-lofar.git


You can install or upgrade Astro-LoFAR from source via

.. code-block:: console

	$ cd astro-lofar
	$ python3 -m pip install -e .

.. tip::

	We suggest to install this package in a virtual environment. If you use a 
	`conda virtual environment <https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`_,
	you still have to use :code:`pip` to install **AstroLoFAR**, 
	so be sure it is the last package to be installed to avoid conflicts with conda packages!


Usage
---------

You can check whether the package has been correctly installed via

.. code-block:: console

	$ python3 -c "import astrolofar"

