.. astrolofar documentation master file, created by
   sphinx-quickstart on Sat Feb  4 19:28:42 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Astro-LoFAR documentation
======================================

.. image:: https://readthedocs.org/projects/example-sphinx-basic/badge/?version=latest
    :target: https://astro-lofar.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status


**Astro-LoFAR** is a Python package to image fits files, extract spectral information, and handle CASA's MS.


.. note::
	
	This project is under active development.


Contents
--------

.. toctree::
    :maxdepth: 2
    
    Home <self>
    installation
    modules
