"""
Tools to manipulate `MeasurementSet <https://casa.nrao.edu/casadocs/casa-5.3.0/reference-material/measurement-set>`_ (MS) files.

You can use the module as

.. code-block:: python

    >>> from astrolofar import ms_tools

"""

__version__ = "0.1.0"

import numpy as np
from casacore.tables import table

def calc_sigma_noise(SEFD, dt, df):
    """
    Calculate the per-visibility noise as

    .. math::
        
        \\sigma = \\frac{SEFD}{\\sqrt{2\\Delta t \\Delta f}} \, ,

    where :math:`SEFD` is the System Equivalent Flux Density (Jy),
    :math:`\\Delta t` is the integration time (seconds), and
    :math:`\\Delta f` is the channel width (Hz).
    
    Parameters
    ----------
    SEFD : float
        The System Equivalent Flux Density in Jy.
    dt : float
        The time reolution (i.e. integration time) of one visibility in seconds.
    df : float
        The frequency resolution (i.e. channel width) of one visibility in Hz.

    Returns
    -------
    sigma : float
        The sigma of the per-visibility thermal noise.

    """
    return SEFD/np.sqrt(2*dt*df)

def make_col(msfile, colname):
    with table(msfile, readonly=False) as mst:
        mst_data = mst.getcol("DATA")
        mst_desc = mst.getcoldesc("DATA")
        mst_desc['name'] = colname
        mst.addcols(mst_desc)
        mst.putcol(colname, np.zeros_like(mst_data))


def add_noise(msfile, sigma, incol="DATA", outcol="DATA"):
    with table(msfile) as mst: colnames = mst.colnames()
    if outcol not in colnames:
        print(outcol, "is not in the MS file")
        print("-> creating", outcol)
        make_col(msfile, outcol)

    with table(msfile, readonly=False) as mst:
        mst_data = mst.getcol(incol)

        print("Adding noise to", incol, "values")
        mst_data += np.random.normal(loc=0, scale=sigma, size=mst_data.shape) + 1j*np.random.normal(loc=0, scale=sigma, size=mst_data.shape)

        print("Saving values into ", outcol)
        mst.putcol(outcol, mst_data)

    return mst_data

if __name__=="__main__":
    print("This is ms_tools.py")

    # test calc_sigma_noise
    print(calc_sigma_noise(4253, 2.003, 183.105e3))
