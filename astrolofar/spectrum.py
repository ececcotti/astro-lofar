"""
Module to extract spectral information from fits files.

.. note::

    This module is built based on `WSClean <https://gitlab.com/aroffringa/wsclean/>`_ fits files,
    i.e. it does not support image cubes (yet). This means you need a fits file for each
    frequency/polarization. 

You can use the module as

.. code-block:: python

    >>> from astrolofar import spectrum
"""

__version__ = "0.1.0"

from datetime import datetime
from tqdm import tqdm

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import LogFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from scipy import optimize

from astropy.wcs import WCS
from astropy.io import fits
from astropy.convolution import Gaussian2DKernel, convolve

from astrolofar.plotfits import *


def gen_mask(fitsname, rms, threshold, sigma_kernel=(0, 0), theta_kernel=0, outname="fitsname"):
    """
    Generate a mask starting from a fits file. The mask has the same pixel scale and size of the
    input fits file, but WCS info are not stored. The mask is 0 in pixels with brightness lower than
    :code:`rms * threshold` and 1 in pixels with brightness higher than :code:`rms * threshold`. 
    A 2D Gaussian smoothing kernel can be applied to avoid sharp pixelized features.

    Parameters
    ----------
    fitsname : str
    Name of the fits file. You may include the full path 
        if the file is not in the directory where you ran Python.
    rms : float
        Standard deviation or RMS value of the background/noise level.
        It must be in the same unit of the data stored in the fits file (e.g. mJy/beam).
    threshold : float
        Set the minimum brightnes included in the mask as :code:`threshold * sigma`.
    sigma_kernel : tuple (float, float), default: (0, 0)
        Standard deviation of the 
        `Gaussian smoothing kernel <https://docs.astropy.org/en/stable/api/astropy.convolution.Gaussian2DKernel>`_ 
        in (x, y) before rotating by :code:`theta`. Default is (0, 0), which means no smoothing is applied.
    theta_kernel : float, default: 0
        Rotation angle of the `Gaussian smoothing kernel <https://docs.astropy.org/en/stable/api/astropy.convolution.Gaussian2DKernel>`_
        in units of radians. The angle increases counter-clockwise.
    outname : str, default: fitsname
        Name of the output mask fits file without extension. If :code:`fitsname` (default) is used, 
        the name of the output file will be the same of the input fits file plus :code:`.mask` 
        (i.e. the final name will be :code:`fitsname.mask.fits`).
    
    Returns
    -------
    mask : 2D-array
        Mask array saved in outname.
    outname : str
        Full name (including extension) of the mask fits file.

    See Also
    --------
    :meth:`astrolofar.plotfits.gen_fits`
    """

    data, _, units = read_fits_data(fitsname)
    
    if sigma_kernel == (0, 0): data_smooth = data
    else:
        kernel = Gaussian2DKernel(x_stddev=sigma_kernel[0], y_stddev=sigma_kernel[1], theta=theta_kernel)
        data_smooth = convolve(data, kernel)
        
    mask_thresh = threshold * rms

    data[data_smooth < mask_thresh] = 0
    data[data_smooth >= mask_thresh] = 1
    
    if outname == "fitsname": outname = fitsname.split(".fits")[0] + ".mask"
    
    gen_fits(data, outname, comments=["Fits mask generated with astrolofar.spectrum.gen_mask", 
        "Threshold = " + str(threshold) + " sigma, where sigma = " + str(rms) + str(units)])

    return data, outname+".fits"

def lin_pol(x, *args):
    summ = 0
    for i in range(len(args)):
        summ += args[i] * (x - 1) ** i
        
    return summ

def log_pol(x, *args):
    """
    Evaluate the logarithmic polynomial using the following formula:
    
    .. math::
        
        y = y_0 x^{c_1} \prod_{i=2}^{n} 10^{c_i \log_{10}^{i}(x)} \, ,
        
    
    
    where :math:`y_0=10^{c_0}` is the normalization factor, :math:`\\alpha=c_1` is the spectral index,
    and :math:`c_i` are the higher orders coefficients (e.g. :math:`\\beta=c_2` is the curvature).
    Using :math:`y_0` instad of :math:`10^{c_0}` allows for the fitting of negative flux densities
    that could be present in real observations, e.g. because of calibration errors. The order of
    the logarithmic polynomial is :math:`n`; e.g., second-order means 3 terms are used, 
    i.e. :math:`c_0,c_1,c_2`.
        

    Parameters
    ----------
    x : array_like
        The bases. If you are using it for spectrum fitting or other astronomycal purpose, 
        it should be the channel frequency divided by a reference frequency: for example,
        :code:`x = freq/150.` where the reference frequency is 150 MHz.
    *args : float
        The coefficients :math:`c_i`. At least two terms must be used, i.e. :math:`c_0` and
        :math:`c_1`.
    
    Returns
    -------
    y : array_like
        The logaritmic polynomial function of `x` of order `n`.

    """
    
    c0 = args[0]
    c1 = args[1]
    
    prod = 1
    if len(args) > 2 : 
        c_terms = args[2:]
        for i in range(len(c_terms)):
            prod *= np.power(10, c_terms[i] * (np.log10(x))**(i+2))
    
    return c0 * np.power(x, c1) * prod


def gen_spix_map(fitslist, nterms=1, reffreq=None, maskfits=None, sigma_kernel=(0, 0), theta_kernel=0, wsclean=False, wsclean_param=None, rescale_target=None, outname="spix"):
    """
    Extract a pixel-by-pixel spectral index map from a set of fits files. 
    A minimum of 2 files must be given as input. Higher order maps can also be generated 
    if :code:`nterms>1`; in this case, at least 3 files must be given in input. 
    Spectral index (and higher order) map is saved in a fits file with the same pixel
    scale, size and `WCS <https://fits.gsfc.nasa.gov/fits_wcs.html>`_ of the input fits.
    
    
    Warning
    -------
    Every fits file must have the same pixel scale and image size. If not, 
    regrid the fits images into the same pixel scale and size. 
    
    Parameters
    ----------
    fitslist : array of str
        List of fits files from which spectral index (and higher orders) will be extracted.
        It must contain more than 2 entries.
    nterms : int, default: 1
        Number of spectral terms for the logaritmic polynomial function. E.g., if `nterms=1` (default)
        only a spectral index map is extracted by fitting a standard power law; if `nterms=2` 
        also a curvature map is produced, using a second-order logarithmic polynomial. 
    reffreq : float, default: None
        Reference frequency (in MHz) at which spectral indices (or higher orders parameters) are evaluated.
        If None (default) is used, `reffreq` will be the central frequency of the fits images 
        in `fitslist`. 
    maskfits : str, default: None
        Mask fits file to extract spectral indices from specific pixels. It can be generated 
        by :meth:`astrolofar.spectrum.gen_mask`. The mask must have the same pixel scale and size
        of the files in `fitslist`. If None (default) is used, spectral indices will be
        extracted from evry pixel of the image.
    sigma_kernel : tuple (float, float), default: (0, 0)
        Standard deviation of the 
        `Gaussian smoothing kernel <https://docs.astropy.org/en/stable/api/astropy.convolution.Gaussian2DKernel>`_ 
        in (x, y) before rotating by :code:`theta`. Default is (0, 0), which means no smoothing is applied.
    theta_kernel : float, default: 0
        Rotation angle of the `Gaussian smoothing kernel <https://docs.astropy.org/en/stable/api/astropy.convolution.Gaussian2DKernel>`_
        in units of radians. The angle increases counter-clockwise.
    wsclean : bool, default: False
        It is only required when the outputs are used in the 
        `forced-spectrum fitting <https://wsclean.readthedocs.io/en/latest/wideband_deconvolution.html#forced-spectral-indices>`_
        of WSClean. When False (dafault) is selected, pixels not selected by the mask assumes NaN values,
        otherwise they get arbitrary values of :math:`-0.7` for the spectral index map
        and :math:`0` for the higher orders terms, which are the average values for the synchrotron radiation.
        An extra label `wsclean` is added to the output fits file.
    outname : str, default: 'spix'
        Name of the output spectral index (and higher order) fits file without extension. 
        A single name must be given: the generated fits file will be a cube with label `SPIX-cube`, 
        where the third axis contain the spectral index map and higher orders 
        (its dimension is the same of `nterms`). If :code:`spix` (default) is used, 
        the output file will be :code:`spix-SPIX-cube.fits`.
        
    Returns
    -------
    si_map : ndarray
        Array with spectral index (and higher order) values.
        
    See Also
    --------
    :meth:`astrolofar.spectrum.gen_mask`
    :meth:`astrolofar.spectrum.log_pol`
    
    """
    
    # make priors
    prior = np.zeros(nterms + 1, dtype=np.float64)
    prior[0] = 100.
    prior[1] = -0.7
    if nterms > 1: prior[2:] = 0.0
    
    
    data0, _, unit = read_fits_data(fitslist[0])  # read reference data
    wcs = read_fits_wcs(fitslist[0])

    sdata = np.zeros((data0.shape[0], data0.shape[1], len(fitslist)), dtype=np.float64)
    freq = np.zeros(len(fitslist), dtype=np.float64)

    if maskfits:
        mask = read_fits_data(maskfits)[0]
        nmask = mask/np.max(mask)
    else: nmask = np.ones( (data0.shape[0], data0.shape[1]) )

    count = 0
    inputlist = ''
    for fitsname in fitslist:
        data = read_fits_data(fitsname)[0] * nmask

        Omega = calc_beam(fitsname)
        tot_flux = np.sum(data) / Omega
        freq[count] = read_fits_data(fitsname)[1] / 1.e6
        print("total flux = %.2f Jy"%tot_flux + " @ %.2f MHz"%freq[count])

        if sigma_kernel == (0, 0): sdata[:, :, count] = data/Omega
        else:
            kernel = Gaussian2DKernel(x_stddev=sigma_kernel[0], y_stddev=sigma_kernel[1], theta=theta_kernel)
            sdata[:, :, count] = convolve(data/Omega, kernel)
        
        # rescale tot_flux to target
        if rescale_target:
            if type(rescale_target) == str:
                if rescale_target.startswith('logpol'): 
                    params = eval(rescale_target.split("logpol")[-1])
                    target = log_pol(freq[count]/params[0], *params[1:])
                    
                elif rescale_target.startswith('linpol'): 
                    params = eval(rescale_target.split("linpol")[-1])
                    target = lin_pol(freq[count]/params[0], *params[1:])
                    
            elif type(rescale_target) == int or type(rescale_target) == float:
                target = rescale_target 
            
            scale_factor = target / tot_flux
            print(f"Scaling factor = {scale_factor:.2f} @ {freq[count]:.2f} MHz (target = {target:.2f} Jy)")

        else: scale_factor = 1
        
        sdata[:, :, count] *= scale_factor                   

        inputlist += fitsname + ', '
        count += 1
    
    inputlist = inputlist[:-2] + ''

    si_map = np.zeros((nterms, data0.shape[1], data0.shape[0]), dtype=np.float64)

    if reffreq == None: reffreq = (freq[-1] + freq[0])/2
    
    
    print("Extracting spectral index map")
    for y in tqdm(range(data0.shape[0])):
        for x in range(data0.shape[1]):
            if nmask[x,y] != 0:
                pars, cov = optimize.curve_fit(f=log_pol, xdata=freq/reffreq, ydata=sdata[x,y,:], p0=prior)
                                
                for n in range(nterms): si_map[n,x,y] = pars[n+1]
    
    for n in range(nterms):
        if wsclean:
            if wsclean_param == None:
                print(f"WSCLEAN MAP: Assign {prior[n+1]} to term c_{n+1}") 
                si_map[n, nmask < np.max(nmask)] = prior[n+1]
            else:
                print(f"WSCLEAN MAP: Assign {wsclean_param[n]} to term c_{n+1}") 
                si_map[n, nmask < np.max(nmask)] = wsclean_param[n]
                
            label = "-cube-wsclean"
        else:
            si_map[n, nmask < np.max(nmask)] = np.nan
            label = "-cube"
    
    gen_fits(si_map, outname+"-SPIX"+label, wcs=wcs, btype='Spectral Index', 
            comments=['Made with astrolofar', 'Reference frequency = '+str(reffreq)+' MHz'])
    
    print("Spectral index map saved in:")
    print(outname+"-SPIX"+label+".fits")
    
    return si_map


if __name__=="__main__":
    print("This is spectrum.py")

    # test gen_mask by making a test case fits file first
    data = np.random.normal(0, 10, size=(256,256))
    x, y = np.ogrid[:data.shape[0], :data.shape[1]]
    
    cntr1 = [100,100] ; r1 = 35
    dist1 = np.sqrt((x - cntr1[0])**2 + (y - cntr1[1])**2)
    cntr2 = [165,150] ; r2 = 56
    dist2 = np.sqrt((x - cntr2[0])**2 + (y - cntr2[1])**2)
    mask0_1 = dist1 <= r1
    mask0_2 = dist2 <= r2
    mask0 = mask0_1 | mask0_2
    data[mask0] += 150

    gen_fits(data, "tests/test_mask")
    fitsname = "tests/test_mask.fits"
    gen_mask(fitsname, rms=10., threshold=10, sigma_kernel=(0,0))

