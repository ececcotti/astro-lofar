"""
Module to plot fits files.

.. note::

    This module is built based on `WSClean <https://gitlab.com/aroffringa/wsclean/>`_ fits files,
    i.e. it does not support image cubes (yet). This means you need a fits file for each
    frequency/polarization. 


You can use the module as

.. code-block:: python

    >>> from astrolofar import plotfits

"""

__version__ = "0.1.0"

from datetime import datetime

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import LogFormatter
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from astropy.wcs import WCS
from astropy.io import fits
from astropy.convolution import Gaussian2DKernel, convolve

from reproject import reproject_exact

def read_fits_data(fitsname):
    """
    Open an existing fits file to extract data and their units 
    using `astropy.io.fits() <https://docs.astropy.org/en/stable/io/fits>`_.

    Parameters
    ----------
    fitsname : str
        Name of the fits file. You may include the full path 
        if the file is not in the directory where you ran Python.
    
    Returns
    -------
    data : 2D-array
        Data saved in the fits file, with shape (`X`,`Y`), 
        where `X` and `Y` are the number of pixels along the x and y axis.
    freq : float
        Frequency or wavelength of the measurement, 
        stored in the fits at the key :code:`CRVAL3`.
    units : str
        Physical data units, stored in the fits at the key :code:`BUNIT`.
    
    Note
    ----
    The fits file should have the keys :code:`BUNIT` and :code:`CRVAL3`. 
    Any WSClean fits file is formatted in the correct way, as well 
    the spectral index map resulting from :py:mod:`astrolofar.spectrum.gen_spix_map`.
    
    """

    with fits.open(fitsname) as hdul:
        data = np.squeeze(hdul[0].data[:])
        if 'BUNIT' in hdul[0].header: units = hdul[0].header['BUNIT']
        else: units = None
        if 'CRVAL3' in hdul[0].header: freq = hdul[0].header['CRVAL3']
        else: freq = None
    return data, freq, units

def read_fits_wcs(fitsname):
    """
    Open an existing fits file to extract `WCS <https://fits.gsfc.nasa.gov/fits_wcs.html>`_ (World Coordinate System).

    Parameters
    ----------
    fitsname : str
        Name of the fits file. You may include the full path 
        if the file is not in the directory where you ran Python.

    Returns
    -------
    WCS object

    Example
    -------
    >>> from astrolofar.plotfits import read_fits_wcs
    >>> fitsname = "../tests/test.fits"    # fits file name
    >>> wcs = read_fits_wcs(fitsname)
    >>> wcs.pixel_to_world(10, 50)
    <SkyCoord (FK5: equinox=2000.0): (ra, dec) in deg
        (299.91265067, 40.70179643)>

    Refer to `Astropy WCS <https://docs.astropy.org/en/stable/wcs>`_ to more information. 
  
    """

    with fits.open(fitsname) as hdul:
        wcs = WCS(hdul[0].header,naxis=2)
    return wcs

def gen_fits(data, outname, wcs=None, freq=None, dfreq=1, btype='None', comments=['Generated with astrolofar']):
    """
    Generate a simple fits file from input data.

    Parameters
    ----------
    data : array_like
        Input array of data.
    outname : str
        Name of the output fits file without extension.
    wcs : WCS object, default: None
        `WCS object <https://docs.astropy.org/en/stable/wcs/>`_ to save sky coordinates 
        in the new fits file. You can use :meth:`astrolofar.plotfits.read_fits_wcs` 
        to extract WCS from an existing fits file that contains them. Default is None.

        Warning
        -------
        The fits file used to extract WCS must be of the same pixel size and scale
        of the new fits file.

    freq : float, default: None
        Reference frequency. If default (None) is used, no frequency axis is added.
    dfreq : float, default: 1
        Frequency increment. It is only activated if `freq` is not None. Default is 1.
    btype : str, default: None
        Unit of data. Default is None.
    comments: list [str], default: ['Generated with astrolofar']
        Any comment that you would like to add in the fits file 
        under the :code:`COMMENT` keyword. Default is 'Generated with astrolofar'.

    Returns
    -------
    outfits : str
        Name of the output fits file (extension is included).

    """
    
    hdr = fits.Header()
    
    hdr['DATE'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    hdr['BTYPE'] = btype
    
    if wcs != None: hdr.update(wcs.to_header())
    
    if freq != None:
        hdr['CTYPE3'] = 'FREQ'
        hdr['CRVAL3'] = freq 
        hdr['CDELT3'] = dfreq
        hdr['CRPIX3'] = 1 
        hdr['CUNIT3'] = 'Hz'
        hdr['NAXIS3'] = 1 
    	
    for comm in comments: hdr['COMMENT'] = comm
    
    hdu = fits.PrimaryHDU(data=data, header=hdr)
    hdu.writeto(outname+".fits", overwrite=True)

    return outname+".fits"


def vrange_percentile(data, q):
    """
    Calculate minimum and maximum value of a `q`-th percentile of the data about the mean value
    using `numpy.percentile() <https://numpy.org/doc/stable/reference/generated/numpy.percentile>`_.

    Parameters
    ----------
    data : array_like
        Input array of data.
    q : float
        Percentile to compute, which must be in the range 0-100 (inclusive). 
        To have a range similar to `DS9 <https://sites.google.com/cfa.harvard.edu/saoimageds9>`_,
        percentiles are calculated with the `qi`-th "image percentile" :math:`qi = q+(100-q)/2`.

    Returns
    -------
    range : tuple (float, float)
        2-item tuple :code:`(vmin, vmax)`, where :code:`vmin = numpy.percentile(data, 100-qi)` and :code:`vmax = numpy.percentile(data, qi)`.

    Example
    -------
    >>> import numpy
    >>> from astrolofar.plotfits import vrange_percentile
    >>> a = numpy.arange(0,201)
    >>> vrange_percentile(a, 95)
    (5.0, 195.0)

    Warning
    -------
    This percentile is different from the "standard" definition of percentile: :code:`numpy.percentile(a, 95)`
    outputs 190 instead of 195 because it is not centered on the mean value of the pixel distribution.
    
    """

    qi = q + (100-q)/2
    vmin, vmax = np.nanpercentile(data, [100-qi,qi])
    return (vmin, vmax)

def vrange_percentile_fits(fitsname, q):
    """
    Use :meth:`astrolofar.plotfits.vrange_percentile` to compute the q-th percentile
    directly from a fits file.

    Parameters
    ----------
    fitsname : str
        Name of the fits file. You may include the full path 
        if the file is not in the directory where you ran Python.
    q : float
        Percentile to compute which must be in the range 0-100 (inclusive). 
        To have a range similar to `DS9 <https://sites.google.com/cfa.harvard.edu/saoimageds9>`_,
        percentiles are calculated with the `qi`-th "image percentile" :math:`qi = q+(100-q)/2`.

    Returns
    -------
    range : tuple (float, float)
        2-item tuple :code:`(vmin, vmax)`, where :code:`vmin = numpy.percentile(data, 100-qi)` and :code:`vmax = numpy.percentile(data, qi)`.


    See Also
    --------
    :meth:`astrolofar.plotfits.vrange_percentile`
    
    """

    data = read_fits_data(fitsname)[0]
    return vrange_percentile(data, q)

def calc_beam(fitsname):
    """
    Calculate the beam size in pixel units.

    Parameters
    ----------
    fitsname : str
        Name of the fits file. You may include the full path 
        if the file is not in the directory where you ran Python.
    
    Returns
    -------
    Omega : float
        Area of the beam in pixel units. 
    
    """
    
    with fits.open(fitsname) as hdul:
        hdr = hdul[0].header
		
    pxl_size = np.abs(hdr['CDELT1']) 
    fwhm_to_sigma = 1. / np.sqrt(8 * np.log(2))
    bmin = hdr['BMIN'] / pxl_size * fwhm_to_sigma
    bmaj = hdr['BMAJ'] / pxl_size * fwhm_to_sigma
	
    if fitsname.endswith("image.fits"): Omega = 2 * np.pi * bmin * bmaj
    elif fitsname.endswith("model.fits"): Omega = 1. 

    return Omega

def plot_fits(fitsname, wcs=None, c_rms=None, c_fitsname=None, c_levels=None, vrange=('min','max'), size=(6,5), trim=(1,1), 
    cmap='viridis', scalelog10=False, cmaplabel=None, savefig=None, outname="fitsname"):
    """
    Plot a fits file setting different parameters.

    Parameters
    ----------
    fitsname : str
        Name of the fits file. You may include the full path
        if the file is not in the directory where you ran Python.
    wcs : WCS object, default: None
        `WCS object <https://docs.astropy.org/en/stable/wcs/>`_ to over-plot sky coordinates. 
        You can use :meth:`astrolofar.plotfits.read_fits_wcs` to extract WCS from an existing fits
        file that contains them. Default is None.
    c_rms : float, default: None
        Standard deviation or RMS of the residuals/background to set the scale
        of contours plot.

        Note
        ----
        This parameters activate contour plotting.
	
    c_fitsname : str, default: None
        Name of the fits file from which countours are extracted. It does not have
        to be the same of `fitsname`. If default (None) is used, :code:`c_fitsname = fitsname`.
        
        Note
        ----
        This parameter does something only if :code:`c_rms` is not None.
	
    c_levels : list, default: None
        List of levels at which countours are plotted. The actual levels are plotted at
        :code:`levels * c_rms`. If deafult (None) is used, `c_levels` is 
        `[2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]`. 
        
        Note
        ----
        This parameter does something only if :code:`c_rms` is not None.
	
    vrange : str or tuple ({str, float}, {str, float}), default: ('min','max')
        Data range that the colormap covers. Supported values are:
        
            1. ('min', 'max') (tuple (str, str)): The default, the colormap covers the full value range of the data.
        
            2. 'Qperc' or 'Q%' (str): The colormap range is computed with the Q-th percentile (see :meth:`astrolofar.plotfits.vrange_percentile`).
        
            3. (`A`, `B`) (tuple (float, float)): The colormap ranges between `A` and `B`, with :math:`A>B`.

            4. Any combination of (1) with (3) (for example, you can set :code:`vrange=('min',100)` or :code:`vrange=(-2.5,'max')`,
            but you can not use :code:`vrange=('10perc',80)`).

    size : tuple (float, float), default: (6, 5)
        Width, height in inches of the full plot.
    trim : tuple (float, float), default: (1, 1)
        Fractions along the x and y axis to crop the image about the image center. A value of 1 means no trim is performed. 
        New ranges are assigned to the axis if different values are assigned:
        :math:`x_{min} = N_{data, x} (1-t_x)/2` and :math:`x_{max} = N_{data, x} (1+t_x)/2`,
        where :math:`t_x` is the input trim value (same for `y`).
    cmap : str or Colormap, default: 'viridis'
        The `Colormap <https://matplotlib.org/stable/api/_as_gen/matplotlib.colors.Colormap>`_ instance or 
        `Matplotlib colormap <https://matplotlib.org/stable/tutorials/colors/colormaps>`_ name used to color the data.
    scalelog10 : bool, default: False
        Format the colormap in log10.
    cmaplabel : str, default: None (default: fits BUNIT)
        Label of the colorbar. If None (default) is used, the :code:`BUNIT` keyword of the input fits file is used. 

        Warning
        -------
        The fits file must have the key :code:`BUNIT`. Any WSClean fits file
        is formatted in the correct way, as well the spectral index map resulting
        from :py:mod:`astrolofar.spectrum.extract_spix`.

	savefig : {'pdf', 'png'}, default: None
        Save the plot in PDF or PNG (with 200 dpi) formats (no plot is showed on screen). 
        If None (default) is used, the plot is not saved on disk but showed on screen.
    outname : str, default: fitsname
        Name of the output image file without extension. Activated only if `savefig` is not None. 
        If :code:`fitsname` (default) is used, the name of the output file will be the same of the input fits file. 

    Returns
    -------
    image : `AxesImage <https://matplotlib.org/stable/api/image_api.html#matplotlib.image.AxesImage>`_ 
    colorbar : `Colorbar <https://matplotlib.org/stable/api/colorbar_api.html#matplotlib.colorbar.Colorbar>`_
    
    """

    mpl.style.use('default')
    mpl.rcParams['xtick.direction'] = 'in'
    mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['xtick.top'] = True
    mpl.rcParams['ytick.right'] = True  
    
    data, freq, units = read_fits_data(fitsname)

    vrange_l = list(vrange)
    if vrange_l[0] == 'min': vrange_l[0] = np.nanmin(data)
    if vrange_l[1] == 'max': vrange_l[1] = np.nanmax(data)
    if type(vrange) == str:
        if 'perc' in vrange.lower():
            perc = vrange.lower().split("perc")[0]
            vrange_l = vrange_percentile(data, float(perc))
        elif '%' in vrange:
            perc = vrange.split("%")[0]
            vrange_l = vrange_percentile(data, float(perc))


    if cmaplabel == None: cmaplabel = units


    fig = plt.figure(figsize=size)

    ax = fig.add_subplot(111, projection=wcs, slices=('x', 'y'))
    ax.coords[0].set_ticklabel(exclude_overlapping=True)
    ax.coords[1].set_ticklabel(exclude_overlapping=True)
    ax.set_xlabel('Right Ascension (J2000)')
    ax.set_ylabel('Declination (J2000)')
    ax.set_aspect('equal')
    ax.set_xlim(int(data.shape[0] * (1-trim[0])//2), int(data.shape[0] * (1+trim[0])//2))
    ax.set_ylim(int(data.shape[1] * (1-trim[1])//2), int(data.shape[1] * (1+trim[1])//2))

    if c_rms != None: 
        if c_fitsname == None: cdata = read_fits_data(fitsname)[0]
        else: cdata = read_fits_data(c_fitsname)[0]
        if c_levels == None: levels = np.array([2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]) * c_rms
        else: levels = np.array(c_levels) * c_rms
        ax.contour(cdata, levels, colors='black', linewidths=0.5, antialiased=False)
    
    axins = inset_axes(ax, width="3%", height="100%", bbox_to_anchor=(.05, 0., 1, 1), bbox_transform=ax.transAxes, borderpad=0)
    if scalelog10:
        formatter = LogFormatter(10, labelOnlyBase=True)
        im = ax.imshow(data, aspect='equal', norm=LogNorm(vmin=vrange_l[0], vmax=vrange_l[1]), cmap=cmap, interpolation=None)
        cb = plt.colorbar(im, cax=axins, format=formatter, label=cmaplabel)
    else:
        im = ax.imshow(data, aspect='equal', vmin=vrange_l[0], vmax=vrange_l[1], cmap=cmap, interpolation=None)
        cb = plt.colorbar(im, cax=axins, label=cmaplabel)

    if outname == 'fitsname': outname = fitsname.split(".fits")[0]
    if type(savefig) == str: 
        if savefig.lower() == 'pdf': plt.savefig(outname+'.pdf', bbox_inches='tight')
        elif savefig.lower() == 'png': plt.savefig(outname+'.png', dpi=200, bbox_inches='tight')
        else: raise Exception('Output format not valid (only pdf or png)')
    else: plt.show()

    plt.close()
    return im, cb

def regrid(fitsname, template, new_size=(512,512)):
    """
    Regrid a fits image to different sizes, using a template fits 
    to transfer WCS information.

    Parameters
    ----------
    fitsname : str
        Name of the fits file. You may include the full path 
        if the file is not in the directory where you ran Python.
    template : str
        Name of the template fits file. This must have the size and
        the pixel scale of the wanted regridded image. 
        It is used to set the correct WCS information in the
        output regridded fits image. Its size must be
        the same of `new_size` to avoid incorrect WCS projections.
    new_size : tuple (int, int), default: (512, 512)
        Image size of the output regridded image in pixels. 
        It must th same of the `template` fits file.

    Returns
    -------
    regridded_data : array_like
        Regridded data. 
    
    """

    wcs_fits = read_fits_wcs(fitsname)
    wcs_template = read_fits_wcs(template)

    data = read_fits_data(fitsname)[0]

    if len(data.shape) > 2:
        regridded_data = np.zeros((data.shape[0], new_size[0], new_size[1]))
        for i in range(data.shape[0]):
            regridded_data[i, ...] = reproject_exact((data[i, ...], wcs_fits), wcs_template, 
                                            shape_out=(new_size[0],new_size[1]),  parallel=8, return_footprint=False)
    else:
        regridded_data = reproject_exact((data, wcs_fits), wcs_template, 
                                shape_out=(new_size[0],new_size[1]),  parallel=8, return_footprint=False)

    return regridded_data



if __name__=="__main__":
    print("This is plotfits.py")

    fitsname = "../tests/test.fits"
    # test read_fits_data()
    data, units = read_fits_data(fitsname)

    # test read_fits_wcs()
    wcs = read_fits_wcs(fitsname)
    #print(wcs.pixel_to_world(10, 50))

    # test vrange_percentile()
    a = np.arange(0,201)
    print(a.min(), a.max())
    print(vrange_percentile(a, 95))

