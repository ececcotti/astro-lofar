"""
Tools for `WSClean <https://gitlab.com/aroffringa/wsclean/>`_ output images.

.. note::

    This module is built based on `WSClean <https://gitlab.com/aroffringa/wsclean/>`_ fits files,
    i.e. it does not support image cubes (yet). This means you need a fits file for each
    frequency/polarization.

You can use the module as

.. code-block:: python

    >>> from astrolofar import wsclean_tools

"""

__version__ = "0.1.0"

from datetime import datetime
from tqdm import tqdm

import sys,os
import subprocess

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from astropy import units as u
from astropy.coordinates import Angle,SkyCoord


from astrolofar.plotfits import *

def read_text_model(text_model):
    with open(text_model) as f:
        header = f.readline().split('Format = ')[1].split('\n')[0].split(', ')
        
        lines = f.readlines()

    # Replace , with ; to avoid issues with lists
    for i in range(len(lines)):
        if ',' in lines[i]:
            lines[i] = lines[i].replace(',', ';')

        # fix , in lists
        if '[' in lines[i] and ']' in lines[i]:
            cidx_all = [j for j, ltr in enumerate(lines[i]) if ltr == ';']
            idx1 = lines[i].find('[')
            idx2 = lines[i].find(']')

            for cidx in cidx_all:
                if idx1 < cidx < idx2:
                    lines[i] = lines[i][:cidx] + ',' + lines[i][cidx+1:]

    # make tmp file with ; instead of ,
    text_model_tmp = text_model + '-tmp'
    with open(text_model_tmp, 'w') as f:
        f.writelines(lines)

    df = pd.read_csv(text_model_tmp, sep=';', header=None, names=header, index_col=False, engine='python')

    # clean tmp files
    subprocess.run(["rm", text_model_tmp])

    return df

def fits_auto_mask(template, text_model):
    data = read_fits_data(template)[0]
    wcs = read_fits_wcs(template)
    df_model = read_text_model(text_model)

    # initialise mask with zeros
    data -= data

    # read RA & Dec of clean components
    for i in range(len(df_model['Dec'])):
        ra_split = df_model['Ra'][i].split(':')
        dec_split = df_model['Dec'][i].split('.')
        cc_ra = Angle(ra_split[0]+'h'+ra_split[1]+'m'+ra_split[2]+'s')
        if cc_ra < 0: cc_ra += Angle('24h')
        if len(dec_split) == 4:
            dec_split[2] = dec_split[2]+'.'+dec_split[3]
        cc_dec = Angle(dec_split[0]+'d'+dec_split[1]+'m'+dec_split[2]+'s')
        x, y = wcs.world_to_pixel(SkyCoord(cc_ra, cc_dec))

        #print(x.round(), y)
        data[int(y.round()), int(x.round())] = 1.

        outname = text_model.split('-sources.txt')[0] + '-automask' 
        gen_fits(data, outname, comments=["Fits mask used by WSClean with auto-mask"])
    
    return data, outname + 'fits'

if __name__=="__main__":
    print("This is wsclean_tools.py")

    # test fits_auto_mask
    text_model = "/data/users/lofareor/emilio/CygA/images_mid/briggs-2/MFS_512x512_0.5asec_briggs-2_noMFw_83ch_scales0-40_thrMask-5-1_spix/cyga-MS-sources.txt"
    #df = read_text_model(text_model)
    template = "/data/users/lofareor/emilio/CygA/images_mid/briggs-2/MFS_512x512_0.5asec_briggs-2_noMFw_83ch_scales0-40_thrMask-5-1_spix/cyga-MS-MFS-model.fits"
    mask, maskname = fits_auto_mask(template, text_model)

    #plt.imshow(mask, origin='lower')
    #plt.show()
    #plt.close()
