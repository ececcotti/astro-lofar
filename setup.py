from setuptools import setup, find_packages

setup(
	name = f'astro-lofar',
	version = '0.1',
	packages=find_packages(),
	description='Collection of functions to image fits files, extract spectral information, and handle MS.'
)
